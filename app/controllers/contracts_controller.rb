class ContractsController < ApplicationController

  def create
    @projects = Project.all
    @workers = Worker.all
    unless params[:worker].nil?
      @contract = Contract.new(worker_id: params[:worker], project_id: params[:project_id])
    end
    unless params[:project].nil?
      @contract = Contract.new(project_id: params[:project], worker_id: params[:worker_id])
    end
    respond_to do |format|
      if @contract.save
        format.js
      else
        flash[:alert] = "#{@contract.errors.full_messages.join(', ')}"
        format.js {render js: "window.location='#{projects_path}'"}
        # format.js {redirect_to projects_path, turbolinks: false}
        # format.html {redirect_to projects_path, turbolinks: false, alert: "#{@contract.errors.full_messages.join(', ')}"}
      end
    end
  end

  def destroy
    @contract = Contract.find(params[:id])
    @contract.destroy
    flash[:notice] = "#{@contract.worker.name} removed from #{@contract.project.name}"
    redirect_to @contract.worker
  end
end

class Contract < ApplicationRecord
  belongs_to :worker
  belongs_to :project
  validates :worker_id, uniqueness: {scope: :project_id, message: "can't work twice in same project"}
end

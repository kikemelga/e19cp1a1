class Worker < ApplicationRecord
  has_many :contracts, dependent: :destroy
  has_many :projects, through: :contracts
end

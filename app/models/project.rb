class Project < ApplicationRecord
  has_many :contracts, dependent: :destroy
  has_many :workers, through: :contracts
  def find_contract(worker_id)
    Contract.where(worker_id: worker_id, project_id: self.id).first
  end
end

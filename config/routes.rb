Rails.application.routes.draw do
  root 'projects#index'
  resources :workers
  resources :projects
  resources :searches, only: [:new]
  resources :contracts, only: [:create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
